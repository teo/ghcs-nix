{ nixImageTag }:

let
  sources = import ./nix/sources.nix;
  baseNixpkgs = import sources.nixpkgs {};
in
with baseNixpkgs;

let
  drvs = import ./.;

  toJob = drvName:
    ''
      build-${drvName}:
        extends: .build
        variables:
          DRV_NAME: "${drvName}"
    '';

  ci-jobs =
    ''
      .build:
        image: nixos/nix:${nixImageTag}
        stage: build
        variables:
          CACHIX_AUTH_TOKEN: unset
        tags:
          - x86_64-linux
        script:
          - ./ci/build.sh
    '' + lib.concatMapStringsSep "\n" toJob (lib.attrNames drvs.ciDrvs);
in { inherit ci-jobs; }
