#!/usr/bin/env nix-shell
#! nix-shell -i bash -p git cachix

set -Eeuo pipefail

cachix use ghc
nix-build . --option cores $CPUS -A ciDrvs.$DRV_NAME | cachix push ghc
