# Build Hadrian via nixpkgs's callCabal2nix
{ lib, haskell, haskellPkgs, shakeVersion, src, alex, happy }:

let
  mkHadrian =
    { mkDerivation, base, bytestring, Cabal, containers
    , directory, extra, filepath, mtl, parsec, QuickCheck, shake
    , stdenv, transformers, unordered-containers
    , alex, happy
    }:
    mkDerivation {
      pname = "hadrian";
      version = "0.1.0.0";
      src = "${src}/hadrian";
      isLibrary = false;
      isExecutable = true;
      executableHaskellDepends = [
        base bytestring Cabal containers directory extra filepath mtl
        parsec QuickCheck shake transformers unordered-containers
      ];
      executableToolDepends = [ alex happy ];
      description = "GHC build system";
      license = lib.licenses.bsd3;
    };

in haskellPkgs.callPackage mkHadrian {
    shake = haskell.lib.dontCheck (haskellPkgs.callHackage "shake" shakeVersion {});
    inherit alex happy;
  }
